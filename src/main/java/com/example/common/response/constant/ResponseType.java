package com.example.common.response.constant;

public enum ResponseType {
    ERROR, OK
}
