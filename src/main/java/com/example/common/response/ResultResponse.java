package com.example.common.response;

import com.example.common.response.constant.ResponseType;
import lombok.Data;

import java.io.Serializable;

@Data
public class ResultResponse implements Serializable {

    public static final String SUCCESS = "SUCCESS";

    private String message;

    private String code;

    private ResponseType type;

    public ResultResponse(){

    }

    public ResultResponse(String message, String code, ResponseType type){
        this.message = message;
        this.code = code;
        this.type = type;
    }
}
