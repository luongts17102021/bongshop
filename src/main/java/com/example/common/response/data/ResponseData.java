package com.example.common.response.data;

import com.example.common.response.ResultResponse;
import com.example.common.response.constant.ResponseType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseData<T> extends ResultResponse {
    private T data;

    public ResponseData() {
        super("SUCCESS", "200", ResponseType.OK);
    }

    public ResponseData(T data) {
        super("SUCCESS", "200", ResponseType.OK);
        this.data = data;
    }
}
