package com.example.common.response.data;

import com.example.common.response.ResultResponse;
import com.example.common.response.constant.ResponseType;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class ResponseListData<T> extends ResultResponse {
    private Collection<T> listData;

    public ResponseListData() {
        super("SUCCESS", "200", ResponseType.OK);
    }

    public ResponseListData(Collection<T> listData) {
        super("SUCCESS", "200", ResponseType.OK);
        this.listData = listData;
    }


}
