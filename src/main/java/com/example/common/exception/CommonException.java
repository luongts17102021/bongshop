package com.example.common.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonException extends Exception {

    public CommonException(String message) {
        this.message = message;
    }

    private String message;

    private String code;


}
