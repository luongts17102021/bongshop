package com.example.common.security.jwt.service;

import com.example.common.response.constant.ResponseType;
import com.example.common.response.data.ResponseData;
import com.example.common.security.jwt.config.JwtUtil;
import com.example.common.security.jwt.dto.CustomUserDetails;
import com.example.common.security.jwt.entity.RefreshToken;
import com.example.common.security.jwt.requset.JwtRequest;
import com.example.common.security.jwt.response.JwtResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JwtAuthenticationService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private RefreshTokenService refreshTokenService;

    public ResponseData<JwtResponse> getToken(JwtRequest request) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));
        } catch (BadCredentialsException e) {
            log.info("Login error by: user name: " + request.getUserName() + " password: " + request.getPassword());
            ResponseData<JwtResponse> responseData = new ResponseData<>();
            responseData.setType(ResponseType.ERROR);
            return responseData;
        }

        CustomUserDetails user = (CustomUserDetails) authentication.getPrincipal();
        String token = jwtUtil.generateToken(user);
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(user.getId());

        return toDomain(token, refreshToken, user);
    }

    private ResponseData<JwtResponse> toDomain(String token, RefreshToken refreshToken, CustomUserDetails user) {
        JwtResponse response = new JwtResponse();
        response.setToken(token);
        response.setRefreshToken(refreshToken.getToken());
        response.setId(user.getId());
        response.setUsername(user.getUsername());
        return new ResponseData<>(response);
    }

}
