package com.example.common.security.jwt.service;


import com.example.common.security.jwt.config.JwtUtil;
import com.example.common.security.jwt.entity.RefreshToken;
import com.example.common.security.jwt.exception.TokenRefreshException;
import com.example.common.security.jwt.repository.RefreshTokenRepository;
import com.example.common.security.jwt.requset.TokenRefreshRequest;
import com.example.common.security.jwt.response.TokenRefreshResponse;
import com.example.source.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;


@Service
public class RefreshTokenService {
    @Value("${security.jwtRefreshExpirationMs:#{3600}}")
    private Long refreshTokenDurationMs;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtUtil jwtUtil;

    public ResponseEntity<TokenRefreshResponse> findByToken(TokenRefreshRequest request) {
        String expiredToken = request.getRefreshToken();
        return refreshTokenRepository.findByToken(expiredToken)
                .map(this::verifyExpiration)
                .map(RefreshToken::getUserEntity)
                .map(user -> {
                    String token = jwtUtil.generateTokenFromUsername(user.getUsername());
                    return ResponseEntity.ok(new TokenRefreshResponse(token, expiredToken));
                })
                .orElseThrow(() -> new TokenRefreshException(expiredToken, "Refresh token is not in database!"));
    }

    public RefreshToken createRefreshToken(Long userId) {
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUserEntity(userRepository.findById(userId).get());
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs * 1000));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenRefreshException(token.getToken(), "Refresh token was expired. Please make a new signin request");
        }

        return token;
    }


    @Transactional
    public int deleteByUserId(Long userId) {
        return refreshTokenRepository.deleteByUserEntity(userRepository.findById(userId).get());
    }
}