package com.example.common.security.jwt.controllers;


import com.example.common.response.data.ResponseData;
import com.example.common.security.jwt.requset.JwtRequest;
import com.example.common.security.jwt.requset.TokenRefreshRequest;
import com.example.common.security.jwt.response.JwtResponse;
import com.example.common.security.jwt.response.TokenRefreshResponse;
import com.example.common.security.jwt.service.JwtAuthenticationService;
import com.example.common.security.jwt.service.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
public class JwtAuthenticationController {


    @Autowired
    private JwtAuthenticationService service;

    @Autowired
    private RefreshTokenService refreshTokenService;


    @PostMapping("/authenticate")
    public  ResponseData<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest request) {
        return service.getToken(request);
    }

    @PostMapping("/refreshToken")
    public ResponseEntity<TokenRefreshResponse> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        return refreshTokenService.findByToken(request);
    }

}
