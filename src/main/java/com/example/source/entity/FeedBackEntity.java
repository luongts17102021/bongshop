package com.example.source.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

//@Getter
//@Setter
//@NoArgsConstructor
//@Entity
//@Table(name = "feedback")
public class FeedBackEntity {

    @Id
    @GeneratedValue
    @Column(name = "feedback_id")
    private long feedBackId;

//    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ProductEntity.class)
//    @JoinColumn(name = "product_id", nullable = false)
//    @JsonIgnore
//    private ProductEntity product;

//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    @JsonIgnore
//    private UserEntity user;

    @Column(name = "comment")
    private String comment;

    @Column(name = "like")
    private long like;

    @Column(name = "status")
    private long status;
}
