package com.example.source.entity;

import com.example.source.Id.CartDetailId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cart_detail")

public class CartDetailEntity {

    @EmbeddedId
    private CartDetailId id;

    @ManyToOne
    @JoinColumn(name = "cart_id", insertable = false, updatable = false)
    @JsonIgnore
    private CartEntity cart;

    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    @JsonIgnore
    private ProductEntity product;

    @Column(name = "quantity")
    private long quantity;
}
