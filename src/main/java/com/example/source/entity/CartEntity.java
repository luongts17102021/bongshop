package com.example.source.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cart")
public class CartEntity {

    @Column(name = "cart_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id

    private long cartId;
    @OneToMany(mappedBy = "cart")
    private Set<CartDetailEntity> cartDetails;

    @OneToOne(mappedBy = "cart")
    private UserEntity user;
}
