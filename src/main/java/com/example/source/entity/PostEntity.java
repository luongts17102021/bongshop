package com.example.source.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "post")
public class PostEntity {

    @Column(name = "post_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long postId;

    @Column(name = "post_name")
    private String postName;

    @Column(name = "post_detail")
    private String postDetail;

    @Column(name = "post_tag")
    private String postTag;

    @Column(name = "status")
    private long status;
}
