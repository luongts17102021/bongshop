package com.example.source.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "category")
public class CategoryEntity {

    @Column(name = "category_id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Id
    private long categoryId;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "category_status")
    private int categoryStatus;

    @OneToMany(mappedBy = "category")
    private Set<ProductEntity> products;

}
