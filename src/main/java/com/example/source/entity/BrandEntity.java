package com.example.source.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "brand")
public class BrandEntity {

    @Column(name = "brand_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long brandId;

    @Column(name = "brand_name")
    private String brandName;

    @OneToMany(mappedBy = "brand")
    private List<ProductEntity> products;
}
