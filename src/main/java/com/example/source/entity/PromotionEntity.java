package com.example.source.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "promotion")
public class PromotionEntity {

    @Column(name = "promotion_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long promotionId;

    @Column(name = "promotion_detail")
    private String promotionDetail;

    @Column(name = "promotion_discount")
    private String promotionDiscount;

}
