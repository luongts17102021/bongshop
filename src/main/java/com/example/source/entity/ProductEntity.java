package com.example.source.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "product")
public class ProductEntity {

    @Column(name = "product_id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Id
    private long productId;

    @Column(name = "product_name")
    private String productName;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private CategoryEntity category;

    @OneToMany(mappedBy = "product")
    private List<OrderDetailEntity> orderDetails;

    @OneToMany(mappedBy = "product")
    private List<CartDetailEntity> cartDetails;

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
//    private Set<FeedBackEntity> feedBacks;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    @JsonIgnore
    private BrandEntity brand;

    @Column(name = "quantity")
    private long quantity;

    @Column(name = "price")
    private long price;

    @Column(name = "origin")
    private String origin;

    @Column(name = "warranty_period")
    private long warrantyPeriod;

    @Temporal(TemporalType.DATE)
    @Column(name = "release_time")
    private Date releaseTime;

    @Column(name = "image")
    private String image;

    @Column(name = "product_status")
    private long productStatus;
}
