package com.example.source.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "user", schema = "public")
@NoArgsConstructor
@Getter
@Setter
public class UserEntity {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @OneToMany(mappedBy = "user")
    private Set<OrderEntity> orders;

//    @OneToMany(mappedBy = "user")
//    private Set<FeedBackEntity> feedbacks;

    @OneToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    @JsonIgnore
    private CartEntity cart;
    @Column(nullable = false, unique = true, name = "user_name")
    private String username;

    @Column( name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column( name = "phone_number")
    private String phoneNumber;

    @Temporal(TemporalType.DATE)
    @Column(name = "create_at")
    private Date createAt;

    @Column(name = "avatar")
    private String avatar;

    @Column(nullable = false, name = "pass_word")
    private String password;
    @Column(nullable = false, name = "roles", columnDefinition = "varchar(255) default 'users'")
    private String role;

    public UserEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserEntity(String username, String email, String address, String phoneNumber, Date createAt, String avatar, String password) {
        this.username = username;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.createAt = createAt;
        this.avatar = avatar;
        this.password = password;
    }
}
