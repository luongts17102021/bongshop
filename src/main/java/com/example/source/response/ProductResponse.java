package com.example.source.response;


import com.example.source.entity.BrandEntity;
import com.example.source.entity.CartDetailEntity;
import com.example.source.entity.OrderDetailEntity;
import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data
public class ProductResponse {

    private String productName;

    private Integer categoryId;
    private Set<OrderDetailEntity> orderDetails;

    private Set<CartDetailEntity> cartDetails;

    private BrandEntity brand;

    private long quantity;

    private long price;

    private String origin;

    private long warrantyPeriod;

    private Date releaseTime;

    private String image;

    private long productStatus;
}
