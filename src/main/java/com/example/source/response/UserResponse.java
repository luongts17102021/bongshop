package com.example.source.response;

import com.example.common.security.jwt.dto.CustomUserDetails;
import lombok.Data;

import java.util.Objects;

@Data
public class UserResponse {
    private String userName;
    private String password;
    private String role;

    public UserResponse(CustomUserDetails userDetails) {
        if (Objects.nonNull(userDetails)) {
            this.userName = userDetails.getUsername();
            this.password = userDetails.getPassword();
            this.role = userDetails.getRole();
        }
    }
}
