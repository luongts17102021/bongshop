package com.example.source.response;

import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;

@Data
public class CategoryReponse {
    private long categoryId;
    private String categoryName;
    private Integer categoryStatus;

    public CategoryReponse(long categoryId, String categoryName, Integer categoryStatus) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryStatus = categoryStatus;
    }
}
