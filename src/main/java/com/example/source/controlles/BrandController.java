package com.example.source.controlles;

import com.example.common.response.ResultResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller("/brand")
public class BrandController {
    @PostMapping("/add-brand")
    public ResultResponse addBrand(){
        return new ResultResponse();
    }
}
