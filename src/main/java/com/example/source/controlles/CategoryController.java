package com.example.source.controlles;

import com.example.common.response.ResultResponse;
import com.example.source.request.CategoryRequest;
import com.example.source.response.CategoryReponse;
import com.example.source.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/search")
    public List<CategoryReponse> search(@RequestBody CategoryRequest request){
        return categoryService.search(request);
    }


    @PostMapping("/add-category")
    public ResultResponse addCategory(@RequestBody CategoryRequest request){
        return  categoryService.addCategory(request);
    }

    @PostMapping("update-category")
    public ResultResponse updateCategory(@RequestBody CategoryRequest request){
        return  categoryService.updateCategory(request);
    }
}
