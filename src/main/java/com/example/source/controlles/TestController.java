package com.example.source.controlles;

import com.example.common.security.jwt.dto.CustomUserDetails;
import com.example.source.response.UserResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class TestController {


    @RequestMapping({"/test"})
    public ResponseEntity<UserResponse> test() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CustomUserDetails user = null;
        if (principal instanceof CustomUserDetails) {
            user = (CustomUserDetails) principal;
        }
        return new ResponseEntity<>(new UserResponse(user), HttpStatus.OK);
    }

}
