package com.example.source.controlles;

import com.example.source.entity.UserEntity;
import com.example.source.repository.UserRepository;
import com.example.source.request.RegisRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class RegisController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody RegisRequest request) throws Exception {
        UserEntity userEntity = request.fromDomain();
        userEntity.setPassword(encoder.encode(userEntity.getPassword()));
        try {
            userRepository.save(userEntity);
        } catch (RuntimeException e) {
            log.info(e.getMessage(), e);
            return new ResponseEntity<>("Account name has exits!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Register success!", HttpStatus.OK);
    }
}
