package com.example.source.controlles;


import com.example.common.response.ResultResponse;
import com.example.source.entity.ProductEntity;
import com.example.source.request.ProductRequest;
import com.example.source.request.ProductSearchRequest;
import com.example.source.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    @PostMapping("/search")
    public List<ProductEntity> search(@RequestBody ProductSearchRequest request){
        return productService.searchProduct(request);
    }

    @PostMapping("/add-product")
    public ResultResponse addProduct(@RequestBody ProductRequest request){
        return productService.addProduct(request);
    }

}
