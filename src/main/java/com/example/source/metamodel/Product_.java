package com.example.source.metamodel;

import com.example.source.entity.ProductEntity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ProductEntity.class)
public abstract class Product_ {
    public static volatile SingularAttribute<ProductEntity, String> productName;
    public static volatile SingularAttribute<ProductEntity, Long> categoryId;
}
