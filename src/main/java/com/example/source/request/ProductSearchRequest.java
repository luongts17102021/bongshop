package com.example.source.request;

import lombok.Data;

import java.util.Date;

@Data
public class ProductSearchRequest {
    private String productName;

    private Integer categoryId;

//    private Integer brandId;
//
//    private long price;
//
//    private long productStatus;
}
