package com.example.source.request;

import lombok.Data;

@Data
public class BrandRequest {
    private long brandId;

    private String brandName;

    private int brandStatus;
}
