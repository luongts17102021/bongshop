package com.example.source.request;

import com.example.source.entity.BrandEntity;
import com.example.source.entity.CartDetailEntity;
import com.example.source.entity.OrderDetailEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ProductRequest {

    private String productName;

    private Integer categoryId;

    private Integer brandId;

    private long quantity;

    private long priceGreaterThan;

    private long priceLessThan;

    private String origin;

    private long warrantyPeriod;

    private Date releaseTime;

    private String image;

    private long productStatus;
}
