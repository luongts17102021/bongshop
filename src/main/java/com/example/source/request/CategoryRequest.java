package com.example.source.request;

import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;

@Data
public class CategoryRequest {
    private long categoryId;
    private String categoryName;
    private Integer categoryStatus;

}
