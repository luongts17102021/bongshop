package com.example.source.request;

import com.example.source.entity.UserEntity;
import lombok.Data;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import java.util.Date;

@Data
public class RegisRequest {
    private String userName;
    private String password;

    public UserEntity fromDomain() throws Exception {
        if (Strings.isNotEmpty(this.userName) && Strings.isNotEmpty(this.password)) {
            UserEntity userEntity = new UserEntity();
            userEntity.setUsername(userName);
            userEntity.setPassword(password);
            userEntity.setRole("userEntity");
            return userEntity;
        }
        throw new Exception("Validated information");
    }
    
}
