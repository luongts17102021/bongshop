package com.example.source.repositorycustorm;

import com.example.source.entity.CategoryEntity;
import com.example.source.request.CategoryRequest;
import com.example.source.response.CategoryReponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryCustomRepository {

    @Autowired
    private EntityManager entityManager;

    public List<CategoryReponse> search(CategoryRequest request) {

        List<CategoryReponse> categoryReponseList = new ArrayList<>();
        Predicate predicateName = null;
        Predicate predicateStatus = null;

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CategoryEntity> criteriaQuery = criteriaBuilder.createQuery(CategoryEntity.class);
        Root<CategoryEntity> fromCategoryTable = criteriaQuery.from(CategoryEntity.class);

        criteriaQuery.select(fromCategoryTable);
        if (StringUtils.isNotEmpty(request.getCategoryName())) {
            predicateName = criteriaBuilder.like(fromCategoryTable.get("categoryName"), "%" + request.getCategoryName() + "%");
        }
        if (null != request.getCategoryStatus()) {
            predicateStatus = criteriaBuilder.equal(fromCategoryTable.get("categoryStatus"), request.getCategoryStatus());
        }
        criteriaQuery.where(predicateName, predicateStatus);
        TypedQuery<CategoryEntity> typedQuery = entityManager.createQuery(criteriaQuery);
        for (CategoryEntity category : typedQuery.getResultList()) {
            categoryReponseList.add(new CategoryReponse(category.getCategoryId(), category.getCategoryName(), category.getCategoryStatus()));
        }
        return categoryReponseList;
    }

}
