package com.example.source.repositorycustorm;

import com.example.source.entity.ProductEntity;
import com.example.source.request.ProductSearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ProductCustomRepository {

    @Autowired
    EntityManager entityManager;

    public List<ProductEntity> searchProduct(ProductSearchRequest request){

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<ProductEntity> criteriaQuery = criteriaBuilder.createQuery(ProductEntity.class);
        Root<ProductEntity> fromProductTable = criteriaQuery.from(ProductEntity.class);
        criteriaQuery.select(fromProductTable);
        setConditionQuery(criteriaQuery,criteriaBuilder,request,fromProductTable);
        TypedQuery<ProductEntity> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }


    public void setConditionQuery(CriteriaQuery<ProductEntity> criteriaQuery, CriteriaBuilder criteriaBuilder, ProductSearchRequest request, Root<ProductEntity> fromProductTable){
        Predicate predicate;
        if(StringUtils.isNotEmpty(request.getProductName())){
            predicate = criteriaBuilder.like(fromProductTable.get("productName"),"%" + request.getProductName() + "%");
            criteriaQuery.where(predicate);
        }

        if(request.getCategoryId() == null){
            predicate = criteriaBuilder.equal(fromProductTable.get("categoryId"),request.getCategoryId());
            criteriaQuery.where(predicate);
        }

    }
}
