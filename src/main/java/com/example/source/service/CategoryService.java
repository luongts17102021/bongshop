package com.example.source.service;

import com.example.common.response.ResultResponse;
import com.example.common.response.constant.ResponseType;
import com.example.source.entity.CategoryEntity;
import com.example.source.repository.CategoryRepository;
import com.example.source.repositorycustorm.CategoryCustomRepository;
import com.example.source.request.CategoryRequest;
import com.example.source.response.CategoryReponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryCustomRepository categoryCustomRepository;

    public List<CategoryReponse> search(CategoryRequest request){
        return categoryCustomRepository.search(request);
    }

    public ResultResponse addCategory(CategoryRequest categoryRequest){
        ResultResponse resultResponse = new ResultResponse();
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setCategoryName(categoryRequest.getCategoryName());
        categoryEntity.setCategoryStatus(categoryRequest.getCategoryStatus());
        categoryRepository.save(categoryEntity);
        resultResponse.setType(ResponseType.OK);
        return resultResponse;
    }

    public ResultResponse updateCategory(CategoryRequest categoryRequest){
        ResultResponse resultResponse = new ResultResponse();
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setCategoryId(categoryRequest.getCategoryId());
        categoryEntity.setCategoryName(categoryRequest.getCategoryName());
        categoryEntity.setCategoryStatus(categoryRequest.getCategoryStatus());
        categoryRepository.save(categoryEntity);
        resultResponse.setType(ResponseType.OK);
        return resultResponse;
    }

}
