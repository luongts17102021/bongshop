package com.example.source.service;


import com.example.common.response.ResultResponse;
import com.example.source.entity.BrandEntity;
import com.example.source.entity.CategoryEntity;
import com.example.source.entity.ProductEntity;
import com.example.source.repositorycustorm.ProductCustomRepository;
import com.example.source.repository.ProductRepository;
import com.example.source.request.ProductRequest;
import com.example.source.request.ProductSearchRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductCustomRepository productCustomRepository;


    public List<ProductEntity> searchProduct(ProductSearchRequest request){
        return productCustomRepository.searchProduct(request);
    }

    public ResultResponse addProduct(ProductRequest request){
        ResultResponse resultResponse = new ResultResponse();
        ProductEntity product = new ProductEntity();
        setProduct(request,product);
        productRepository.save(product);
        return resultResponse;
    }

    public void setProduct(ProductRequest request, ProductEntity product){
        product.setProductName(request.getProductName());
        CategoryEntity category = new CategoryEntity();
        category.setCategoryId(request.getCategoryId());
        product.setCategory(category);
        product.setImage(null);
        product.setOrigin(request.getOrigin());
        product.setPrice(product.getPrice());
        product.setQuantity(request.getQuantity());
        product.setReleaseTime(request.getReleaseTime());
        product.setWarrantyPeriod(request.getWarrantyPeriod());
        BrandEntity brand = new BrandEntity();
        brand.setBrandId(request.getBrandId());
        product.setBrand(brand);
    }

}
